<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function get_nick($id){
        $user                           = static :: where('id',$id)->first();
        if($user){
            return $user->name;
        }
    }

    public static function getByNick($nick){
        return static :: where('name',$nick)->first();
    }

    public function getAvatarAttribute($avatar){
        return get_image_url($avatar);
    }
}
