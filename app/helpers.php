<?php
/**
 * Author: XiaoFei Zhai
 * Date: 2017/12/30
 * Time: 10:27
 */

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

if (!function_exists('user_nick')) {
    function user_nick($id)
    {
        return \App\User::get_nick($id);
    }
}
if (!function_exists('get_image_url')) {
    function get_image_url($path)
    {
        if (URL::isValidUrl($path)) {
            return $path;
        }
        if(!empty($path)){
            return Storage::disk(config('admin.upload.disk'))->url($path);
        }

    }
}
function local_time_format($date)
{
//    if (Carbon::now() > Carbon::parse($date)->addDays(10)) {
    //    return Carbon::parse($date);
//    }
    return Carbon::parse($date)->diffForHumans();
}
function avatar($avatar){
    return !empty($avatar) ? $avatar : asset('images/default_avatar.png');
}

