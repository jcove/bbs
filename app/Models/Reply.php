<?php
/**
 * Author: XiaoFei Zhai
 * Date: 2017/12/28
 * Time: 17:27
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    public function author()
    {
        return $this->belongsTo('App\User','author_id');
    }

    public static function children($id){
        $list                       =   static :: where('comment_id',$id)->get();
        if($list){
            foreach ($list as $k => $v){
                if(!empty($v->reply_id)){
                    foreach ($list as $item){
                        if($v->reply_id==$item->id){
                            $v->reply       =   $item;
                            $list[$k]       =   $v;
                            break;
                        }
                    }
                }
            }
        }
        return $list;
    }
}