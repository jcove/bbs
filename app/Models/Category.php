<?php
/**
 * Author: XiaoFei Zhai
 * Date: 2018/1/16
 * Time: 10:21
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public static function options(){
        return static::where(['hidden'=>0])->get();
    }
    public static function allNav($status = 1){
        return static ::where(['status'=>$status])->get();
    }
}