<?php
/**
 * User: XiaoFei Zhai
 * Date: 17/9/27
 * Time: 下午5:58
 */

namespace App\Http\Controllers;


use App\Models\File;
use Encore\Admin\Form\Field\UploadField;
use Illuminate\Http\Request;

class FileController extends Controller
{
    use UploadField;

    public function upload(Request $request){

        if ($request->hasFile('file')) {

            if($request->file('file')->isValid()){

                $md5                                =   md5_file($request->file('file'));


                $oldFile                            =   File::getByMd5($md5);
                if($oldFile){
                    return $oldFile;
                }else{
                    $this->initStorage();
                    $fileUpload                     =   $request->file('file');
                    $file                           =   new File();
                    $path = $this->storage->putFileAs($this->getDirectory(), $fileUpload, $fileUpload->hashName());
                    $file->original_name            =   $request->file->getClientOriginalName();
                    $file->path                     =   $path;
                    $file->ext                      =   $request->file->getClientOriginalExtension();
                    $file->name                     =   $request->file->hashName();
                    $file->md5                      =   $md5;
                    $file->save();
                    return $file;
                }

            }else{
                return "file error";
            }
        }else{
            return "file not exits";
        }
    }

    public function defaultDirectory()
    {
        return config('admin.upload.directory.image');
    }
}