<?php
/**
 * Author: XiaoFei Zhai
 * Date: 2018/2/3
 * Time: 9:38
 */

namespace App\Http\Controllers;


use App\Http\Requests\UpdateUserPut;
use App\Models\Article;
use App\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function profile($id){
        $user                               =   User::where(['id'=>$id])->first();
        $data['user']                       =   $user;
        $data['articles']                   =   Article::getByAuthor($id);
        $data['title']                      =   $user->name.' 个人中心';
        return view('user.profile',$data);

    }

    public function articles($id){
        $user                               =   User::where(['id'=>$id])->first();
        $data['user']                       =   $user;
        $data['articles']                   =   Article::where(['status'=>1,'author_id'=>$id])->orderBy('updated_at','desc')->paginate(20);
        $data['title']                      =   $user->name.' 全部文章';
        return view('user.articles',$data);
    }

    public function edit($id){

        $data['user']                       =   Auth::user();
        $data['title']                      =   '编辑个人资料';
        return view('user.edit',$data);
    }

    public function update(UpdateUserPut $request){
        $user                               =   Auth::user();
        if($request->email!=$user->email){
            $user->email                    =   $request->email;
        }
        if($request->nick != $user->nick){
            $user->nick                    =   $request->nick;
        }
        if($request->gender != $user->gender){
            $user->nick                    =   $request->nick;
        }
        if($request->introduction != $user->introduction){
            $user->introduction                    =   $request->introduction;
        }
        if($request->signature != $user->signature){
            $user->signature                    =   $request->signature;
        }
        if($request->gender != $user->gender){
            $user->gender                    =   $request->gender;
        }
        if(get_image_url($request->avatar) != $user->avatar){
            $user->avatar                    =   $request->avatar;
        }
        $user->save();
        return back();
    }
}