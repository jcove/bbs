<?php
/**
 * Author: XiaoFei Zhai
 * Date: 2018/2/7
 * Time: 14:07
 */

namespace app\Http\Controllers;


use App\Models\File;
use Encore\Admin\Form\Field\UploadField;
use Illuminate\Http\Request;

class UeditorController
{
    use UploadField;
    public function server(Request $request)
    {
        $config = config('ueditor.upload');
        $action = $request->get('action');
        switch ($action) {
            case 'config':
                $result = $config;
                break;
            case 'uploadimage':
                $result = $this->response($this->upload($request));
                break;

        }
        return response()->json($result, 200, [], JSON_UNESCAPED_UNICODE);
    }
    protected function upload(Request $request){
        if ($request->hasFile('upfile')) {

            if($request->file('upfile')->isValid()){
                $md5                                =   md5_file($request->file('upfile'));
                $oldFile                            =   File::getByMd5($md5);
                if($oldFile){
                    return $oldFile;
                }else{
                    $this->initStorage();
                    $fileUpload                     =   $request->file('upfile');
                    $file                           =   new File();
                    $path = $this->storage->putFileAs($this->getDirectory(), $fileUpload, $fileUpload->hashName());
                    $file->original_name            =   $request->upfile->getClientOriginalName();
                    $file->path                     =   $path;
                    $file->ext                      =   $request->upfile->getClientOriginalExtension();
                    $file->name                     =   $request->upfile->hashName();
                    $file->md5                      =   $md5;
                    $file->save();
                    return $file;
                }

            }else{
                return null;
            }
        }else{
            return null;
        }
    }
    public function response($file){
        if($file){
            return array(
                "state" => 'SUCCESS',
                "url" => '/uploads/'.$file->path,
                "title" => $file->name,
                "original" => $file->original_name,
                "type" => $file->ext,
                "size" => $file->size
            );
        }else{
            return array(
                "state" => 'ERROR_UNKNOWN',
                "url" => '',
                "title" => '',
                "original" => '',
                "type" => '',
                "size" => ''
            );
        }

    }
    public function defaultDirectory()
    {
        return config('admin.upload.directory.image');
    }

}