<?php
/**
 * Author: XiaoFei Zhai
 * Date: 2018/1/20
 * Time: 13:51
 */

namespace App\Http\Controllers;


use App\Models\Article;
use App\Models\Category;

class CategoryController extends Controller
{
    public function articles($slug){
        $category                               =   Category::where(['slug'=>$slug])->first();
        if($category){
            $list                               =   Article::where(['status'=>1])->where('category_id',$category->id)->orderBy('top','des')->orderBy('created_at','desc')->paginate(20);
            $data['title']                      =   $category->name;
            $data['list']                       =   $list;
        }
        return view('category.articles',$data);
    }
}