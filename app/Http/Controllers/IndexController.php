<?php
/**
 * Author: XiaoFei Zhai
 * Date: 2017/12/25
 * Time: 15:09
 */

namespace App\Http\Controllers;


use App\Models\Article;

class IndexController extends Controller
{
    public function index(){
        $data['list']                   =   $this->news();
        $data['title']                  =   '首页';
        return view('index.index',$data);
    }

    public function news(){
        $list                           =   Article::where('status',1)->orderBy('top','des')->orderBy('updated_at','desc')->paginate(20);
        return $list;
    }
}