<?php
/**
 * Author: XiaoFei Zhai
 * Date: 2017/12/28
 * Time: 17:31
 */

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Models\Article;
use App\Models\Comment;
use App\Models\Reply;
use App\User;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function comment(CommentRequest $request){
        $comment                            =   new Comment();
        $comment->article_id                =   $request->article_id;
        $comment->body                      =   $request->body;
        $comment->author_id                 =   Auth::id();
        $comment->comment_id                =   $request->comment_id ? $request->comment_id : 0;
        preg_match('/@\S+\s/',$comment->body,$match);
        if($match && isset($match[0]) && !empty($match)){
            $user                           =   User::getByNick(str_replace_first('@','',$match[0]));
            $comment->replay_author_id      =   $user->id;
            $comment->body                  =   str_replace_first($match[0],'',$comment->body);
        }
        $comment->save();
        Article::comment($comment);
        $list                               =   $this->getCommentList($request->article_id);
        $data['list']                       =   $list;
        $data['article_id']                 =   $request->article_id;
        admin_toastr('回复成功');
        return back();
    }

    public function lists($article_id){
        $list                               =   $this->getCommentList($article_id);
        $data['list']                       =   $list;
        $data['article_id']                 =   $article_id;
        return view('comment.lists',$data);
    }

    protected function getCommentList($articleId){
        $list                               =   Comment::where('article_id',$articleId)->orderBy('created_at','desc')->paginate(10);
        if(!empty($list)){
            $items                          =   $list->items();
            foreach ($items as $key=> $value){
                $items[$key]->replies       =   Reply::children($value->id);
            }
        }
        return $list;
    }

    public function reply(){
        $request                            =   \request();
        $reply                              =   new Reply();
        $reply->body                        =   $request->body;
        $reply->author_id                   =   Auth::id();
        $reply->comment_id                  =   $request->comment_id ? $request->comment_id : 0;
        $reply->reply_id                    =   $request->reply_id ? $request->reply_id : 0;
        $reply->save();

        return redirect()->back();
    }
}