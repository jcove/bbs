<?php

namespace App\Http\Middleware;
use Closure;


class Nav
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $navs                                   =   \App\Models\Nav::allNav(1);

        view()->share('navs',$navs);
        return $next($request);
    }
}
