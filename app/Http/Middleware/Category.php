<?php
/**
 * Author: XiaoFei Zhai
 * Date: 2018/2/1
 * Time: 17:10
 */

namespace app\Http\Middleware;

use Closure;

class Category
{
    public function handle($request, Closure $next)
    {

        $categories                                   =   \App\Models\Category::options();
        view()->share('categories',$categories);
        return $next($request);
    }
}