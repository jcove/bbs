<ul class="list-group comment-list row">
    @foreach($list as $item)
        <li class="list-group-item media" style="margin-top: 0px;">
            <div class="avatar avatar-container pull-left">
                <a href="">
                    <img class="media-object img-thumbnail avatar avatar-middle" alt="{{$item->author->name}}"
                         src="{{$item->author->avatar or asset('images/default_avatar.png')}}"
                         style="width:55px;height:55px;">
                </a>
            </div>
            <div class="infos">
                <div class="media-heading">
                    <a href="" title="DavidNineRoc"
                       class="remove-padding-left author rm-link-color">
                        {{$item->author->name}}
                    </a>
                    <span class="introduction"></span>
                    <span class="operate pull-right">
                         <a class="fa fa-reply" href="javascript:void(0)" onclick="replyOne('{{$item->author->name}}');"
                            title="回复">
                             <i class="iconfont icon-comment" style="font-size:14px;"></i>
                         </a>
                    </span>
                    <div class="meta">
                        <a name="reply34639" id="reply34639" class="anchor" href="#reply34639"
                           aria-hidden="true">#{{$loop->index+1}}</a>
                        <span> ⋅  </span>
                        <abbr class="timeago"
                              title="{{$item->created_at}}">{{ local_time_format($item->created_at)}}</abbr>
                    </div>
                </div>

                <div class="media-body markdown-reply content-body">
                    <p>
                        @if(!empty($item->replayAuthor))
                            <a href="">@ {{ $item->replayAuthor->name }}</a> @endif{{$item->body}}
                    </p>
                </div>

            </div>
        </li>
    @endforeach
</ul>

<script>
    function replyOne(username) {
        replyContent = $("#reply_content");
        oldContent = replyContent.val();
        let prefix = "@" + username + " ";
        newContent = ''
        if (oldContent.length > 0) {
            if (oldContent != prefix) {
                newContent = oldContent + "\n" + prefix;
            }
        } else {
            newContent = prefix
        }
        replyContent.focus();
        replyContent.val(newContent);
        moveEnd($("#reply_content"));
    }

    var moveEnd = function (obj) {
        obj.focus();

        var len = obj.value === undefined ? 0 : obj.value.length;

        if (document.selection) {
            var sel = obj.createTextRange();
            sel.moveStart('character', len);
            sel.collapse();
            sel.select();
        } else if (typeof obj.selectionStart == 'number' && typeof obj.selectionEnd == 'number') {
            obj.selectionStart = obj.selectionEnd = len;
        }
    }
</script>

