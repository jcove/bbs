<li id="article-list-{{$item->id}}" class="article-item list-group-item">
    <a class="reply_count_area hidden-xs pull-right" href="{{route('article.show',['id'=>$item->id])}}">
        <div class="count_set">
            <span class="count_of_replies" title="回复数">{{$item->comments or  0}}</span>
            <span class="count_seperator">/</span>
            <span class="count_of_visits" title="查看数">{{$item->view}}</span>
            <span class="count_seperator">|</span>
            <abbr title="{{$item->created_at}}" class="timeago">{{local_time_format($item->updated_at)}}</abbr>
        </div>
    </a>
    <div class="author">
        <img class="avatar" src="{{$item->author->avatar or asset('images/default_avatar.png')}}">
    </div>
    <div class="article-list-item-body">
        <div class="media-heading">
            @if($item->top > 0)
                <span class="hidden-xs label label-warning">置顶</span>
            @endif
            <a class="link" href="{{route('article.show',['id'=>$item->id])}}" title="{{$item->title}}">
                {{$item->title}}
            </a>
        </div>
    </div>
</li>
