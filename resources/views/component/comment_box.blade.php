<div class="comment-box">
    <div class="do-comment">
        <form class="form-horizontal" comment-pjax action="{{url('/comment')}}" method="post">
            <div class="form-group">
                <div class="col-xs-12">
                    <textarea class="form-control" rows="3" placeholder="发表评论"></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <input name="article_id" type="hidden" value="{{$post->id}}">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-success pull-right">提交</button>
                </div>
            </div>
        </form>
    </div>
    <div class="comment-lists">
        <ul>
            @foreach($list as $item)
                <li class="comment-item">
                    <div class="header">
                    </div>
                    <div class="body">
                        {{$item->body}}
                    </div>
                    <div class="comment-metadata">

                    </div>
                </li>
            @endforeach
        </ul>
    </div>

</div>