@extends('layouts.default')
@section('body')
    <div class="row">
        <div class="col-md-12">
            <div class="category-bar col-md-3">
                <ul class="list-group">
                    @foreach($categories as $category)
                        <li class="list-group-item">
                            <a href="{{route('category-articles',['slug'=>$category->slug])}}">
                                <i class="iconfont icon-{{$category->slug}} color{{$loop->index}}"></i><span
                                        class="name">{{$category->name}}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
            @if(count($list) > 0)
                <div class="article-lists col-md-9">
                    <ul class="list-group">
                        @foreach($list as $item)
                            @include('component.post_item')
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
    <div class="pull-right add-padding-vertically">
        {{ $list->links() }}
    </div>

@endsection