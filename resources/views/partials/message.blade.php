@if(Session::has('success'))
    <script>
        <?php $success = Session::get('success');?>
        message({
            title: "{{ array_get($success->get('title'), 0) }}",
            text: "{!!  array_get($success->get('message'), 0) !!}",
            type: "success",
            confirmButtonText: "Cool"
        });
    </script>
@endif