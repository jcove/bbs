<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{$title or ''}} {{config('app.name', 'Dms')}} - Powered by Dms</title>
    <meta name="keywords" content="@yield('keywords', config('site.site_keywords'))"/>
    <meta name="description" content="@yield('description', config('site.site_description'))"/>
    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <script src="http://apps.bdimg.com/libs/jquery/1.9.0/jquery.min.js"></script>
    <script>
        Config = {
            'routes': {
                'upload_image' : '{{ route('file.upload') }}'
            },
            'token': '{{ csrf_token() }}',
            'environment': '{{ app()->environment() }}',

        };
    </script>

    @yield('style')
</head>
<body>
@include('component.header')
@include('partials.error')
@include('component.body')
@include('component.footer')



<script src="{{ admin_asset ("/vendor/laravel-admin/sweetalert/dist/sweetalert.min.js") }}"></script>
<script src="{{ admin_asset ("/vendor/laravel-admin/toastr/build/toastr.min.js") }}"></script>
<script src="{{ mix('js/app.js') }}"></script>

@include('partials.success')
@include('partials.exception')
@include('partials.toastr')
@yield('script')
<script>
    var _hmt = _hmt || [];
    (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?17bdb9f02d3ad33a38cc4c754466b187";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>
</body>
</html>
