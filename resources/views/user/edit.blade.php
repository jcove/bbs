@extends('layouts.default')
@section('body')
    <div class="users-show">

        <div class="col-md-3 main-col">
            <div class="box">
                <div class="padding-md ">
                    <div class="list-group text-center">
                        <a href="{{route('user.edit',['id'=>Auth::user()->id])}}" class="list-group-item active">
                            <i class="text-md iconfont icon-listalt" aria-hidden="true"></i>
                            &nbsp;个人信息
                        </a>
                        <a href="" class="list-group-item ">
                            <i class="text-md iconfont icon-22BELL" aria-hidden="true"></i>
                            &nbsp;消息通知
                        </a>

                        <a href="" class="list-group-item ">
                            <i class="text-md iconfont icon-lock" aria-hidden="true"></i>
                            &nbsp;修改密码
                        </a>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-md-9  left-col ">

            <div class="panel panel-default padding-md">

                <div class="panel-body ">

                    <h2><i class="iconfont icon-setting" aria-hidden="true"></i> 编辑个人资料</h2>
                    <hr>


                    <form class="form-horizontal" method="POST" action="{{route('user.update',['id'=>Auth::user()->id])}}"
                          accept-charset="UTF-8" enctype="multipart/form-data">
                        <input name="_method" value="PUT" type="hidden">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">用户名</label>
                            <div class="col-sm-6">
                                <input class="form-control" disabled value="{{Auth::user()->name}}" type="text">
                            </div>

                            <div class="col-sm-4 help-block">

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">昵称</label>
                            <div class="col-sm-6">
                                <input class="form-control" name="nick" value="{{Auth::user()->nick}}" type="text">
                            </div>

                            <div class="col-sm-4 help-block">
                                取个个性的昵称
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">头像</label>
                            <div class="col-sm-6">
                                @component('component.picture_upload',['path'=> !empty(Auth::user()->avatar) ? Auth::user()->avatar : '','name'=>'avatar'])
                                @endcomponent
                            </div>

                            <div class="col-sm-4 help-block">

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">性别</label>
                            <div class="col-sm-6">
                                <select class="form-control" id="gender" name="gender">
                                    <option value="unselected" selected="">未选择</option>
                                    <option value="male">男</option>
                                    <option value="female">女</option>
                                </select>
                                <script>
                                    $("#gender option[value={{Auth::user()->gender}}]").attr("selected", "selected");
                                </script>
                            </div>

                            <div class="col-sm-4 help-block"></div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">邮 箱</label>
                            <div class="col-sm-6">
                                <input class="form-control" name="email" value="{{Auth::user()->email}}" type="text">
                            </div>
                            <div class="col-sm-4 help-block">
                                如：name@website.com
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">个人简介</label>
                            <div class="col-sm-6">
                                <textarea class="form-control" rows="3" name="introduction" cols="50"
                                          style="overflow: hidden; overflow-wrap: break-word; resize: horizontal; height: 121px;"></textarea>
                            </div>
                            <div class="col-sm-4 help-block">
                                请一句话介绍你自己，大部分情况下会在你的头像和名字旁边显示
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">个性签名</label>
                            <div class="col-sm-6">
                                <textarea class="form-control" rows="3" name="signature" cols="50"
                                          style="overflow: hidden; overflow-wrap: break-word; resize: horizontal; height: 121px;"></textarea>
                            </div>
                            <div class="col-sm-4 help-block">
                                个性签名，会拼接在每一个你发表过的文章内容后面。
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-6">
                                <input class="btn btn-primary" id="user-edit-submit" value="应用修改" type="submit">
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>


    </div>
@endsection