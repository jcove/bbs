@extends('layouts.default')
@section('body')
    <div class="users-show  row">
        <div class="col-md-3">
            <div class="box">
                <div class="padding-sm user-basic-info">
                    <div style="">
                        <div class="media">
                            <div class="media-left">
                                <div class="image">
                                    <img class="media-object avatar-112 avatar img-thumbnail"
                                         src="{{avatar($user->avatar)}}">
                                </div>
                            </div>
                            <div class="media-body">
                                <h3 class="media-heading">
                                    {{$user->name}}
                                </h3>
                                <div class="item">
                                    <svg width="14" height="16" viewBox="0 0 14 14" class="Icon Icon--male"
                                         aria-hidden="true"
                                         style="height: 16px; width: 14px;fill:#9fadc7;top: 2px;position: relative;">
                                        <title></title>
                                    </svg>
                                </div>
                                <div class="item number">
                                    注册于 <span class="timeago">{{ local_time_format($user->created_at)}}</span>
                                </div>

                                <div class="item number">
                                    活跃于 <span class="timeago">10小时前</span>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>
        <div class="main-col col-md-9 left-col">
            <div class="box text-center">A Life-long learner.</div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    文章
                </div>
                <div class="panel-body">
                    <ul class="list-group">
                        @if(!empty($articles))
                            @foreach($articles as $article)
                                <li class="list-group-item">
                                    <a href="{{route('article.show',['id'=>$article->id])}}" title="{{$article->title}}">
                                        {{$article->title}}
                                    </a>
                                    <span class="meta">
                                        <span> ⋅ </span>
                                        {{$article->comments}}
                                        <span> ⋅ </span>
                                        <span class="timeago">{{local_time_format($article->updated_at)}}</span>
                                  </span>
                                    @if(Auth::id()==$article->author_id)
                                        <a class="btn btn-success btn-sm pull-right" href="{{route('article.edit',['id'=>$article->id])}}">编辑</a>
                                    @endif
                                </li>
                                @endforeach
                        @endif
                    </ul>
                    <div class="panel-footer" style="margin-top: 10px">
                        <a href="{{route('user.articles',['id'=>$user->id])}}" class="btn btn-default btn-sm">
                            所有文章
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection