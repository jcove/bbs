<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/home', 'HomeController@index')->name('home');
Route::get('/test', 'TestController@index')->name('test');
Route::group(['middleware' => 'auth'], function () {
    Route::post('/comment', 'CommentController@comment')->name('comment');
    Route::post('/comment/reply', 'CommentController@reply')->name('reply');
    Route::post('/file/upload', 'FileController@upload')->name('file.upload');
    Route::any('/ueditor', 'UeditorController@server')->name('ueditor');
});
Route::group(['middleware' => ['nav','category']], function () {

    Auth::routes();
    Route::get('/', 'IndexController@index');
    Route::get('/category/{slug?}', 'CategoryController@articles')->name('category-articles');
    Route::resource('article', 'ArticleController');
    Route::get('/user/profile/{id}', 'UserController@profile')->name('user.profile');
    Route::get('/user/{id}/articles', 'UserController@articles')->name('user.articles');
    Route::get('/user/{id}/edit', 'UserController@edit')->name('user.edit')->middleware('auth');
    Route::put('/user/{id}', 'UserController@update')->name('user.update')->middleware('auth');;
});

Route::get('/comment/lists/{article_id?}', 'CommentController@lists')->name('comment_lists');
