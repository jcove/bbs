<?php
/**
 * User: XiaoFei Zhai
 * Date: 17/10/18
 * Time: 下午3:30
 */
return [
    'site_title'                =>  env('SITE_TITLE'),//网站关键词
    'site_keywords'             =>  env('SITE_KEYWORDS'),//网站关键词
    'site_description'          =>  env('SITE_DESCRIPTION'),//网站描述
];